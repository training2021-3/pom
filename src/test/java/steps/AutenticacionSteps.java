package steps;

import com.co.sofkau.web.controllers.BCSouceDemo;
import com.co.sofkau.web.controllers.DriverController;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class AutenticacionSteps {

    WebDriver driver;

    @Before
    public void setup() {
        driver = DriverController.getDriver();
    }


    @Dado("^un usuario en la pagina inicial de souce demo$")
    public void unUsuarioEnLaPaginaInicialDeSouceDemo() {
        BCSouceDemo.startApp(driver, "https://www.saucedemo.com/");
    }

    @Cuando("^el usuario ingresa un \"([^\"]*)\" y \"([^\"]*)\" correctos$")
    public void elUsuarioIngresaUnYCorrectos(String username, String password) {
        BCSouceDemo.loginUser(driver, username, password);
    }

    @Entonces("^se autentica en el sitio correctamente$")
    public void seAutenticaEnElSitioCorrectamente() {
        Assert.assertEquals(BCSouceDemo.getTitleHome(driver), "PRODUCTS");
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
